create table sprint_reviews (
	id bigint NOT NULL AUTO_INCREMENT,
	create_date timestamp not null, 
	reviewer_id bigint not null,
	sprint_id bigint not null,
	primary key (id),
	FOREIGN KEY (reviewer_id) REFERENCES users(id),
	FOREIGN KEY (sprint_id) REFERENCES sprints(id)
);

create table retrospective_items (
	id bigint NOT NULL AUTO_INCREMENT,
	create_date timestamp not null, 
	name varchar(255) not null,
	description varchar(500) not null,
	visible bit not null default true,
	primary key (id)
);

create table sprint_review_items (
	id bigint NOT NULL AUTO_INCREMENT,
	create_date timestamp not null, 
	sprint_review_id bigint not null,
	retro_item_id bigint not null,
	score int not null,
	primary key (id),
	FOREIGN KEY (sprint_review_id) REFERENCES sprint_reviews(id),
	FOREIGN KEY (retro_item_id) REFERENCES retrospective_items(id)
);