create table system_settings (
	id bigint NOT NULL AUTO_INCREMENT,
	create_date timestamp not null, 
	current_sprint_id bigint,
	primary key (id),
	FOREIGN KEY (current_sprint_id) REFERENCES sprints(id)
);