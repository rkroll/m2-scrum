create table teams (
	id bigint NOT NULL AUTO_INCREMENT, 
	create_date timestamp not null, 
	name varchar(100), 
	scrum_master_id bigint not null, 
	primary key (id), 
	unique (name),
	FOREIGN KEY (scrum_master_id) REFERENCES users(id)
);

create table team_user(
	team_id bigint NOT NULL,
	user_id bigint NOT NULL,
	FOREIGN KEY (team_id) REFERENCES teams(id),
	FOREIGN KEY (user_id) REFERENCES users(id)
);
