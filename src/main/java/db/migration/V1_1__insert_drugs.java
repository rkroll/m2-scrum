//package db.migration;
//
//import org.flywaydb.core.api.migration.spring.SpringJdbcMigration;
//import org.springframework.jdbc.core.JdbcTemplate;
//
//public class V1_1__insert_drugs implements SpringJdbcMigration {
//
//	@Override
//	public void migrate(JdbcTemplate jdbcTemplate) throws Exception {
//		jdbcTemplate.execute("INSERT INTO drugs (name, loinc_code) VALUES ('1,2-pentanediol (bulk)', '129122')");
//	}
//
//}
