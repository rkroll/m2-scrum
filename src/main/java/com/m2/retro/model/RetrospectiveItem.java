package com.m2.retro.model;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.util.Assert;

import com.m2.core.model.AbstractModel;

@Entity
@Table(name = "retrospective_items")
public class RetrospectiveItem extends AbstractModel {

	private static final long serialVersionUID = 4779023409667098805L;

	private String name;

	private String description;

	private Boolean visible;

	RetrospectiveItem() {
		// for hibernate
	}

	public RetrospectiveItem(String name, String description) {
		Assert.notNull(name, "A name is required");
		Assert.notNull(description, "A description is required");
		this.name = name;
		this.description = description;
		this.visible = Boolean.TRUE;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	@Override
	public String toString() {
		return "RetrospectiveItem [name=" + name + ", description=" + description + ", visible=" + visible + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((description == null) ? 0 : description.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((visible == null) ? 0 : visible.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		RetrospectiveItem other = (RetrospectiveItem) obj;
		if (description == null) {
			if (other.description != null)
				return false;
		} else if (!description.equals(other.description))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (visible == null) {
			if (other.visible != null)
				return false;
		} else if (!visible.equals(other.visible))
			return false;
		return true;
	}

}
