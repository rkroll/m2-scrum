package com.m2.retro.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.m2.core.model.AbstractModel;
import com.m2.profile.model.User;
import com.m2.sprint.model.Sprint;

@Entity
@Table(name = "sprint_reviews")
public class SprintReview extends AbstractModel {

	private static final long serialVersionUID = 2761821916363383663L;

	private User reviewer;
	private Sprint sprint;
	private List<SprintReviewItem> items;

	@OneToOne
	@JoinColumn(name = "reviewer_id")
	public User getReviewer() {
		return reviewer;
	}

	public void setReviewer(User reviewer) {
		this.reviewer = reviewer;
	}

	@ManyToOne
	@JoinColumn(name = "sprint_id")
	public Sprint getSprint() {
		return sprint;
	}

	public void setSprint(Sprint sprint) {
		this.sprint = sprint;
	}

	@OneToMany(mappedBy = "review")
	public List<SprintReviewItem> getItems() {
		return items;
	}

	public void setItems(List<SprintReviewItem> items) {
		this.items = items;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((items == null) ? 0 : items.hashCode());
		result = prime * result + ((reviewer == null) ? 0 : reviewer.hashCode());
		result = prime * result + ((sprint == null) ? 0 : sprint.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SprintReview other = (SprintReview) obj;
		if (items == null) {
			if (other.items != null)
				return false;
		} else if (!items.equals(other.items))
			return false;
		if (reviewer == null) {
			if (other.reviewer != null)
				return false;
		} else if (!reviewer.equals(other.reviewer))
			return false;
		if (sprint == null) {
			if (other.sprint != null)
				return false;
		} else if (!sprint.equals(other.sprint))
			return false;
		return true;
	}

}
