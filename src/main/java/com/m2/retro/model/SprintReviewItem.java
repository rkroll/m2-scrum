package com.m2.retro.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.m2.core.model.AbstractModel;

@Entity
@Table(name = "sprint_review_items")
public class SprintReviewItem extends AbstractModel {

	private static final long serialVersionUID = 8183165830384404568L;

	@NotNull
	private SprintReview review;
	
	@Min(0)
	@Max(5)
	private int score;
	
	@NotNull
	private RetrospectiveItem retrospectiveItem;

	@ManyToOne
	@JoinColumn(name = "sprint_review_id")
	public SprintReview getReview() {
		return review;
	}

	public void setReview(SprintReview review) {
		this.review = review;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@OneToOne
	@JoinColumn(name = "retro_item_id")
	public RetrospectiveItem getRetrospectiveItem() {
		return retrospectiveItem;
	}

	public void setRetrospectiveItem(RetrospectiveItem retrospectiveItem) {
		this.retrospectiveItem = retrospectiveItem;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((retrospectiveItem == null) ? 0 : retrospectiveItem.hashCode());
		result = prime * result + ((review == null) ? 0 : review.hashCode());
		result = prime * result + score;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SprintReviewItem other = (SprintReviewItem) obj;
		if (retrospectiveItem == null) {
			if (other.retrospectiveItem != null)
				return false;
		} else if (!retrospectiveItem.equals(other.retrospectiveItem))
			return false;
		if (review == null) {
			if (other.review != null)
				return false;
		} else if (!review.equals(other.review))
			return false;
		if (score != other.score)
			return false;
		return true;
	}

}
