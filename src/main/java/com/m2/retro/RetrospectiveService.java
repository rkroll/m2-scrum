package com.m2.retro;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.m2.profile.UserService;
import com.m2.profile.model.User;
import com.m2.retro.dto.RetroItemDTO;
import com.m2.retro.model.RetrospectiveItem;
import com.m2.retro.model.SprintReview;
import com.m2.retro.repository.RetroSpecifications;
import com.m2.retro.repository.RetrospectiveItemRepository;
import com.m2.retro.repository.SprintReviewItemRepository;
import com.m2.retro.repository.SprintReviewRepository;
import com.m2.sprint.model.Sprint;

@Service
@Transactional
public class RetrospectiveService {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private SprintReviewRepository sprintReviewRepository;

	@Autowired
	private SprintReviewItemRepository sprintReviewItemRepository;

	@Autowired
	private RetrospectiveItemRepository retrospectiveItemRepository;

	public RetrospectiveItem createRetroItem(RetroItemDTO retroItemDTO) {
		RetrospectiveItem item = new RetrospectiveItem(retroItemDTO.getName(), retroItemDTO.getDescription());
		return getRetrospectiveItemRepository().save(item);
	}
	
	public Boolean reviewedSprint(Sprint sprint) {
		User user = getUserService().getCurrentUser();
		//SprintReview review = sprintReviewRepository.findOne(where(user(user)).and(sprint(sprint)));
		//return review != null;
		return false;
	}

	public SprintReviewRepository getSprintReviewRepository() {
		return sprintReviewRepository;
	}

	public void setSprintReviewRepository(SprintReviewRepository sprintReviewRepository) {
		this.sprintReviewRepository = sprintReviewRepository;
	}

	public SprintReviewItemRepository getSprintReviewItemRepository() {
		return sprintReviewItemRepository;
	}

	public void setSprintReviewItemRepository(SprintReviewItemRepository sprintReviewItemRepository) {
		this.sprintReviewItemRepository = sprintReviewItemRepository;
	}

	public RetrospectiveItemRepository getRetrospectiveItemRepository() {
		return retrospectiveItemRepository;
	}

	public void setRetrospectiveItemRepository(RetrospectiveItemRepository retrospectiveItemRepository) {
		this.retrospectiveItemRepository = retrospectiveItemRepository;
	}

	public List<RetrospectiveItem> findActive() {
		return retrospectiveItemRepository.findAll(RetroSpecifications.retroItemVisible());
	}

	
	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
