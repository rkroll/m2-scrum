package com.m2.retro.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.m2.retro.model.RetrospectiveItem;

public class RetroSpecifications {

	public static Specification<RetrospectiveItem> retroItemVisible() {
		return new Specification<RetrospectiveItem>() {

			@Override
			public Predicate toPredicate(Root<RetrospectiveItem> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("visible"), true);
			}
		};
	}
}
