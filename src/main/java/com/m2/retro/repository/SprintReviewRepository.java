package com.m2.retro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.m2.retro.model.SprintReview;

public interface SprintReviewRepository  extends JpaRepository<SprintReview, Long>, JpaSpecificationExecutor<SprintReview> {

}
