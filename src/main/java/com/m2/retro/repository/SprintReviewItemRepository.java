package com.m2.retro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.m2.retro.model.SprintReviewItem;

public interface SprintReviewItemRepository extends JpaRepository<SprintReviewItem, Long>, JpaSpecificationExecutor<SprintReviewItem> {

}
