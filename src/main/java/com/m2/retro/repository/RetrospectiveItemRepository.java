package com.m2.retro.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.m2.retro.model.RetrospectiveItem;

public interface RetrospectiveItemRepository extends JpaRepository<RetrospectiveItem, Long>, JpaSpecificationExecutor<RetrospectiveItem> {

}
