package com.m2.system.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.m2.system.model.SystemSettings;

public interface SystemSettingsRepository extends JpaRepository<SystemSettings, Long> {
	@Query("select s from SystemSettings s where s.currentSprint != null")
	SystemSettings getSystemSettings();
}
