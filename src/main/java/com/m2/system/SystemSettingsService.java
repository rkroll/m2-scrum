package com.m2.system;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.stereotype.Service;

import com.m2.system.model.SystemSettings;
import com.m2.system.repository.SystemSettingsRepository;

@Service
public class SystemSettingsService {

	@Autowired
	private SystemSettingsRepository systemSettingsRepository;

	public SystemSettings getSystemSettings() {
		List<SystemSettings> settings = systemSettingsRepository.findAll();

		if (settings.isEmpty()) {
			throw new IllegalStateException("System settings do not exist.");
		} else {
			return DataAccessUtils.singleResult(settings);
		}
	}

	public SystemSettings save(SystemSettings systemSettings) {
		return systemSettingsRepository.save(systemSettings);
	}
}
