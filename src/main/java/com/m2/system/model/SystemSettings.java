package com.m2.system.model;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.m2.core.model.AbstractModel;
import com.m2.sprint.model.Sprint;

@Entity
@Table
public class SystemSettings extends AbstractModel {
	private static final long serialVersionUID = -2370488766153032845L;

	private Sprint currentSprint;

	@OneToOne
	@JoinColumn(name = "current_sprint_id")
	public Sprint getCurrentSprint() {
		return currentSprint;
	}

	public void setCurrentSprint(Sprint currentSprint) {
		this.currentSprint = currentSprint;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((currentSprint == null) ? 0 : currentSprint.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SystemSettings other = (SystemSettings) obj;
		if (currentSprint == null) {
			if (other.currentSprint != null)
				return false;
		} else if (!currentSprint.equals(other.currentSprint))
			return false;
		return true;
	}

}
