package com.m2.security;

import java.util.Arrays;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import com.m2.profile.model.RoleEnum;

@Service
public class SecurityService {
	public void doAsAdmin(SecurityCallback callback) {
		Authentication origAuth = SecurityContextHolder.getContext().getAuthentication();

		GrantedAuthority admin = new SimpleGrantedAuthority(RoleEnum.ROLE_ADMIN.name());

		Authentication authRequest = new UsernamePasswordAuthenticationToken("admin", "test",
				Arrays.asList(new GrantedAuthority[] { admin }));
		SecurityContextHolder.getContext().setAuthentication(authRequest);

		try {
			callback.execute();
		} finally {
			SecurityContextHolder.getContext().setAuthentication(origAuth);
		}
	}
}
