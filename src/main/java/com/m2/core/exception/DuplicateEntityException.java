package com.m2.core.exception;

/**
 * Thrown to signial an entity with the name/id already exists.
 * 
 * @author richkroll
 * 
 */
public class DuplicateEntityException extends RuntimeException {

	private static final long serialVersionUID = -1374735287774648847L;

	public DuplicateEntityException() {
		super();
	}

	public DuplicateEntityException(String message, Throwable cause) {
		super(message, cause);
	}

	public DuplicateEntityException(String message) {
		super(message);
	}

	public DuplicateEntityException(Throwable cause) {
		super(cause);
	}

}
