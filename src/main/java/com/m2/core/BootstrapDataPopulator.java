package com.m2.core;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.m2.profile.UserService;
import com.m2.profile.model.Role;
import com.m2.profile.model.RoleEnum;
import com.m2.profile.model.User;
import com.m2.profile.repository.RoleRepository;
import com.m2.security.SecurityCallback;
import com.m2.security.SecurityService;
import com.m2.system.model.SystemSettings;
import com.m2.system.repository.SystemSettingsRepository;
import com.m2.web.admin.user.UserForm;

@Component
public class BootstrapDataPopulator implements InitializingBean {

	private static final Logger logger = LoggerFactory.getLogger(BootstrapDataPopulator.class);

	@Autowired
	private UserService userService;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private SecurityService securityService;

	@Autowired
	private SystemSettingsRepository systemSettingsRepository;

	@Override
	public void afterPropertiesSet() throws Exception {
		logger.info("Adding bootstrap data");

		securityService.doAsAdmin(new SecurityCallback() {
			@Override
			public void execute() {
				for (RoleEnum r : RoleEnum.values()) {
					Role role = roleRepository.findByName(r.name());

					if (role == null) {
						role = new Role(r.name());
						roleRepository.save(role);
					}
				}

				roleRepository.flush();

				User admin = getUserService().findByUsername("admin");

				if (admin == null) {
					UserForm userForm = new UserForm();
					userForm.setEmail("admin@modmed.com");
					userForm.setUsername("admin");
					userForm.setName("Admin");
					userForm.setPassword("changeme");
					userForm.setRoles(Lists.newArrayList(RoleEnum.ROLE_ADMIN.toString()));

					userService.create(userForm);
				}

				List<SystemSettings> settings = systemSettingsRepository.findAll();

				if (settings.isEmpty()) {
					SystemSettings systemSettings = new SystemSettings();
					systemSettingsRepository.save(systemSettings);
				}
			}
		});

		logger.info("Bootstrap data complete");
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public RoleRepository getRoleRepository() {
		return roleRepository;
	}

	public void setRoleRepository(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

}
