package com.m2.core.model;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

@MappedSuperclass
@XmlType
public abstract class AbstractModel implements Serializable {

	private static final long serialVersionUID = -5255761558194669290L;

	protected transient final Logger LOG = LoggerFactory.getLogger(getClass());

	private Date createDate = new Date();

	private Long id;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the createDate
	 */
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", nullable = false)
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public abstract boolean equals(Object obj);

	public abstract int hashCode();

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder().append(getClass().getSimpleName()).append("[");
		String k = "ID";

		Method method = BeanUtils.findMethod(getClass(), "getId");
		if (method == null) {
			k = "Name";
			method = BeanUtils.findMethod(getClass(), "getName");
		}

		result.append(k + ":");

		if (method != null) {
			try {
				result.append(method.invoke(this));
			}
			catch (Exception e) {
				if (LOG.isErrorEnabled()) {
					LOG.error("While serializing to string", e);
				}
			}
		}
		result.append("]");

		return result.toString();
	}

}