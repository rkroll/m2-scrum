package com.m2.core.model;

import java.util.Date;

public interface DateRangeAware {
	Date getStartDate();
	Date getEndDate();
}
