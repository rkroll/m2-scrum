package com.m2.sprint;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.m2.core.exception.DuplicateEntityException;
import com.m2.sprint.dto.SprintDTO;
import com.m2.sprint.model.Sprint;
import com.m2.sprint.repository.SprintRepository;
import com.m2.system.SystemSettingsService;
import com.m2.system.model.SystemSettings;

@Service
@Transactional
public class SprintService implements MessageSourceAware {

	private MessageSource messageSource;

	@Autowired
	private SprintRepository sprintRepository;

	@Autowired
	private SystemSettingsService systemSettingsService;

	public Sprint getCurrentSprint() {
		SystemSettings settings = systemSettingsService.getSystemSettings();
		return settings.getCurrentSprint();
	}

	public void makeCurrentSprint(Sprint sprint) {
		Assert.notNull(sprint, "A sprint is required");
		SystemSettings settings = systemSettingsService.getSystemSettings();
		settings.setCurrentSprint(sprint);
		systemSettingsService.save(settings);
	}

	public Page<Sprint> findActive(Pageable pageable) {
		return sprintRepository.findByVisibleTrue(pageable);
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Sprint create(SprintDTO sprintDTO) {
		Assert.notNull(sprintDTO, "A sprint is required");
		Sprint sprint = new Sprint(sprintDTO.getName());
		BeanUtils.copyProperties(sprintDTO, sprint, "id");

		sprint = save(sprint);

		if (Boolean.TRUE.equals(sprintDTO.getMakeCurrent())) {
			makeCurrentSprint(sprint);
		}
		
		return sprint;
	}

	@PreAuthorize("hasRole('ROLE_ADMIN')")
	public Sprint save(SprintDTO sprintDTO) {
		Assert.notNull(sprintDTO, "Sprint DTO is required");
		Sprint sprint = sprintRepository.getOne(sprintDTO.getId());
		BeanUtils.copyProperties(sprintDTO, sprint, "id");

		sprint = save(sprint);

		if (Boolean.TRUE.equals(sprintDTO.getMakeCurrent())) {
			makeCurrentSprint(sprint);
		}

		return sprint;
	}

	private Sprint save(Sprint sprint) {
		try {
			return sprintRepository.save(sprint);
		} catch (DataIntegrityViolationException e) {
			String msg = getMessageSource().getMessage("name.exists.error", new Object[] { "Sprint" }, LocaleContextHolder.getLocale());
			throw new DuplicateEntityException(msg);
		}
	}

	public Sprint getOne(Long id) {
		return sprintRepository.getOne(id);
	}

	/**
	 * @return the messageSource
	 */
	public MessageSource getMessageSource() {
		return messageSource;
	}

	/**
	 * @param messageSource
	 *            the messageSource to set
	 */
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	/**
	 * @return the sprintRepository
	 */
	public SprintRepository getSprintRepository() {
		return sprintRepository;
	}

	/**
	 * @param sprintRepository
	 *            the sprintRepository to set
	 */
	public void setSprintRepository(SprintRepository sprintRepository) {
		this.sprintRepository = sprintRepository;
	}

	public SystemSettingsService getSystemSettingsService() {
		return systemSettingsService;
	}

	public void setSystemSettingsService(SystemSettingsService systemSettingsService) {
		this.systemSettingsService = systemSettingsService;
	}

}
