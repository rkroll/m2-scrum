package com.m2.sprint.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.m2.sprint.model.Sprint;

public class SprintSpecifications {
	public static Specification<Sprint> nameIs(final String name) {
		return new Specification<Sprint>() {
			@Override
			public Predicate toPredicate(Root<Sprint> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("name"), name);
			}
		};
	}

	public static Specification<Sprint> active() {
		return new Specification<Sprint>() {
			@Override
			public Predicate toPredicate(Root<Sprint> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("visible"), true);
			}
		};
	}
}
