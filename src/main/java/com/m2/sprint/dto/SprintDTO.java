package com.m2.sprint.dto;

import java.io.Serializable;
import java.util.Date;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import com.m2.core.model.DateRangeAware;
import com.m2.sprint.model.Sprint;
import com.m2.validation.TodayOrFuture;

public class SprintDTO implements Serializable, DateRangeAware {

	private static final long serialVersionUID = -3475255261464464393L;

	public interface Creating extends Default {
	}

	public interface Updating extends Default {
	}

	@NotNull(groups = { Updating.class })
	private Long id;

	@NotNull
	private String name;

	@NotNull
	private Boolean visible = true;

	@NotNull
	@TodayOrFuture
	private Date startDate = null;

	@Future
	private Date endDate = null;

	private Boolean makeCurrent;

	public SprintDTO() {
	}

	public SprintDTO(Sprint sprint) {
		this.id = sprint.getId();
		this.name = sprint.getName();
		this.visible = sprint.isVisible();
		this.startDate = sprint.getStartDate();
		this.endDate = sprint.getEndDate();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Boolean getVisible() {
		return visible;
	}

	public void setVisible(Boolean visible) {
		this.visible = visible;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public Boolean getMakeCurrent() {
		return makeCurrent;
	}

	public void setMakeCurrent(Boolean makeCurrent) {
		this.makeCurrent = makeCurrent;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((makeCurrent == null) ? 0 : makeCurrent.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + ((visible == null) ? 0 : visible.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SprintDTO other = (SprintDTO) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		} else if (!endDate.equals(other.endDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (makeCurrent == null) {
			if (other.makeCurrent != null)
				return false;
		} else if (!makeCurrent.equals(other.makeCurrent))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (visible == null) {
			if (other.visible != null)
				return false;
		} else if (!visible.equals(other.visible))
			return false;
		return true;
	}

}
