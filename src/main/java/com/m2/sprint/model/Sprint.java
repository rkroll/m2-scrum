package com.m2.sprint.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;

import com.m2.core.model.AbstractModel;
import com.m2.core.model.DateRangeAware;
import com.m2.validation.TodayOrFuture;
import com.m2.validation.ValidDateRange;

@Entity
@Table(name = "sprints")
@ValidDateRange
public class Sprint extends AbstractModel implements DateRangeAware {

	private static final long serialVersionUID = 2743317840905340143L;

	@NotNull
	private String name;

	private boolean visible = true;

	@NotNull
	@TodayOrFuture
	private Date startDate;

	@Future
	private Date endDate;

	Sprint() {
		// for hibernate
	}

	public Sprint(String name) {
		this.name = name;
		this.startDate = new Date();
	}

	/**
	 * End this sprint
	 */
	public void end() {
		this.visible = false;
		this.endDate = new Date();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Is this sprint visible
	 */
	public boolean isVisible() {
		return visible;
	}

	/**
	 * @param visible the visible to set
	 */
	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	/**
	 * The date the sprint starts.
	 * 
	 * @return the start date
	 */
	public Date getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the start date of the sprint
	 */
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	/**
	 * The date the sprint ends.
	 * 
	 * @return the sprint end date
	 */
	public Date getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the date the sprint ends.
	 */
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + (visible ? 1231 : 1237);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sprint other = (Sprint) obj;
		if (endDate == null) {
			if (other.endDate != null)
				return false;
		}
		else if (!endDate.equals(other.endDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		}
		else if (!name.equals(other.name))
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		}
		else if (!startDate.equals(other.startDate))
			return false;
		if (visible != other.visible)
			return false;
		return true;
	}

}
