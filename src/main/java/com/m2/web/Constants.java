package com.m2.web;

public class Constants {
	public static final String SUCCESS_MESSAGE_KEY = "successMessage";
	public static final String ERROR_MESSAGE_KEY = "errorMessage";
	
	public static final int COMMENTS_CHAR_MAX = 500;
	
	public static final String USER_SECTION = "user";
	public static final String TEAM_SECTION = "team";
	public static final String SPRINT_SECTION = "sprint";
	public static final String RETRO_ITEM_SECTION = "retro-item";
	public static final String ADMIN_SECTION = "admin";
}
