package com.m2.web.common;

public class I18nKeys {
	public static final String UPDATE_SUCCESS = "update.success";
	public static final String UPDATE_ERROR = "update.error";
	public static final String USER = "user.label";
	public static final String TEAM = "team.label";
	public static final String SPRINT = "sprint.label";  
	public static final String RETRO_ITEM = "retro_item.label";
	
	public static final String ROLE = "role.label";
	public static final String USERNAME = "user.username";
	
	public static final String CREATE_SUCCESS = "create.success";
	public static final String REMOVE_SUCCESS = "remove.success";
	public static final String SAVE_SUCCESS = "save.success";
	
}                  