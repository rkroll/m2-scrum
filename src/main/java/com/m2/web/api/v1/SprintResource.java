package com.m2.web.api.v1;

import java.net.URI;

import javax.validation.Valid;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.m2.jersey.param.IntParam;
import com.m2.sprint.SprintService;
import com.m2.sprint.dto.SprintDTO;
import com.m2.sprint.model.Sprint;

@Path("/sprint")
@Component
@Produces(MediaType.APPLICATION_JSON)
@Scope("request")
public class SprintResource {

	@Autowired
	private SprintService sprintService;

	@Context
	UriInfo uriInfo;

	@GET
	@Path("/{id}")
	public Response getSprint(@PathParam("id") Long id) {
		Sprint sprint = sprintService.getOne(id);
		return Response.ok(sprint).build();
	}

	@GET
	@Path("/")
	public Response getActiveSprints(@QueryParam("page") @DefaultValue("0") IntParam page,
			@QueryParam("size") @DefaultValue("10") IntParam size) {
		PageRequest pageRequest = new PageRequest(page.get(), size.get());
		Page<Sprint> sprints = getSprintService().findActive(pageRequest);
		return Response.ok(sprints).build();
	}

	@POST
	@Path("/")
	public Response createSprint(@Valid SprintDTO sprintDTO) {
		Sprint sprint = sprintService.create(sprintDTO);
		URI sprintId = uriInfo.getRequestUriBuilder().path("/{id}").build(sprint.getId());
		return Response.created(sprintId).entity(sprint).build();
	}

	/**
	 * @return the sprintService
	 */
	public SprintService getSprintService() {
		return sprintService;
	}

	/**
	 * @param sprintService
	 *            the sprintService to set
	 */
	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}

}
