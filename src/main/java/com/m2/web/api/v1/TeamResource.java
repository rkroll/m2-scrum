package com.m2.web.api.v1;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.m2.web.api.v1.model.Message;

@Path("/hello")
public class TeamResource {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Message message() {
		return new Message("Hello World");
	}
}
