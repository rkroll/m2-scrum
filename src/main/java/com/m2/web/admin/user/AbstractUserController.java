package com.m2.web.admin.user;

import org.springframework.beans.factory.annotation.Autowired;

import com.m2.profile.UserService;
import com.m2.web.AbstractBaseController;
import com.m2.web.Constants;

public class AbstractUserController extends AbstractBaseController {

	@Autowired
	private UserService userService;

	public AbstractUserController() {
		super(Constants.USER_SECTION);
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

}
