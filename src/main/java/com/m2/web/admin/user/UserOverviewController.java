package com.m2.web.admin.user;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.m2.profile.model.User;
import com.m2.web.Views;

@Controller
@RequestMapping("/admin/users")
public class UserOverviewController extends AbstractUserController {

	@ModelAttribute("users")
	public List<User> addUsers() {
		return getUserService().findAll();
	}

	@RequestMapping
	public String usersOverview() {
		return Views.USER_OVERVIEW;
	}
}
