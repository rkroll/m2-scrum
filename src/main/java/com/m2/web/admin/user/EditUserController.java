package com.m2.web.admin.user;

import java.util.List;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.m2.profile.model.Role;
import com.m2.profile.model.User;
import com.m2.web.Constants;
import com.m2.web.Views;
import com.m2.web.common.I18nKeys;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@Controller
@RequestMapping("/admin/users")
public class EditUserController extends AbstractUserController {

	@ModelAttribute("userForm")
	public UserForm userForm(@PathVariable("id") Long id) {
		User user = getUserService().findOne(id);
		return new UserForm(user);
	}

	@ModelAttribute("roles")
	public List<Role> roles() {
		return getUserService().getRoles();
	}

	@RequestMapping("/{id}")
	public String displayForm() {
		return Views.USER_EDIT;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String saveUser(@Validated(UserForm.Updating.class) UserForm userForm, BindingResult result, RedirectAttributes redirectAttr) {
		String resultString = Views.USER_EDIT;

		if (!result.hasErrors()) {
			try {
				getUserService().save(userForm);
				String msg = getMessage(I18nKeys.UPDATE_SUCCESS, getMessage(I18nKeys.USER));
				redirectAttr.addFlashAttribute(Constants.SUCCESS_MESSAGE_KEY, msg);
				resultString = "redirect:/admin/users";
			} catch (DataIntegrityViolationException e) {
				result.addError(new FieldError("userForm", "username", "Must be Unique"));
			}
		}

		return resultString;
	}
}
