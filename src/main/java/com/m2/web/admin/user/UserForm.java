package com.m2.web.admin.user;

import java.util.List;

import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import com.google.common.collect.Lists;
import com.m2.profile.model.Role;
import com.m2.profile.model.User;
import com.m2.validation.ValidUsername;

public class UserForm {
	public interface Creating extends Default {
	}

	public interface Updating extends Default {
	}

	// only valide presence of ID when validating updates
	@NotNull(groups = { Updating.class })
	Long id;

	@NotEmpty
	@Email
	private String email;

	@NotEmpty
	@ValidUsername
	private String username;

	@NotEmpty
	private String name;

	@Length(min = 3, max = 255, groups = { Creating.class })
	@NotEmpty(groups = { Creating.class })
	private String password;

	@NotEmpty
	private List<String> roles = Lists.newArrayList();

	public UserForm() {

	}

	public UserForm(User user) {
		this.id = user.getId();
		this.email = user.getEmail();
		this.username = user.getUsername();
		this.name = user.getName();

		for (Role role : user.getRoles()) {
			this.roles.add(role.getName());
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<String> getRoles() {
		return roles;
	}

	public void setRoles(List<String> roles) {
		this.roles = roles;
	}

}
