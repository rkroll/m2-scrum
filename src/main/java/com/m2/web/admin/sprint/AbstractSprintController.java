package com.m2.web.admin.sprint;

import org.springframework.beans.factory.annotation.Autowired;

import com.m2.sprint.SprintService;
import com.m2.web.AbstractBaseController;
import com.m2.web.Constants;

public class AbstractSprintController extends AbstractBaseController {

	@Autowired
	private SprintService sprintService;

	public AbstractSprintController() {
		super(Constants.SPRINT_SECTION);
	}

	public SprintService getSprintService() {
		return sprintService;
	}

	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}

}
