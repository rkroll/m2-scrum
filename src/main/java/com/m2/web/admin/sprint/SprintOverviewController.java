package com.m2.web.admin.sprint;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.m2.sprint.model.Sprint;
import com.m2.web.Views;

@Controller
@RequestMapping("/admin/sprints")
public class SprintOverviewController extends AbstractSprintController {
	@ModelAttribute("sprints")
	public Page<Sprint> addSprints(Pageable pageable) {
		return getSprintService().findActive(pageable);
	}

	@ModelAttribute("currentSprint")
	public Sprint addCurrentSprint() {
		return getSprintService().getCurrentSprint();
	}

	@RequestMapping
	public String overview() {
		return Views.SPRINT_OVERVIEW;
	}
}
