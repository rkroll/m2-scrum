package com.m2.web.admin.sprint;

import javax.validation.Valid;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.m2.sprint.dto.SprintDTO;
import com.m2.web.Constants;
import com.m2.web.Views;
import com.m2.web.common.I18nKeys;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@Controller
@RequestMapping("/admin/sprints")
public class NewSprintController extends AbstractSprintController {

	@ModelAttribute("sprintDTO")
	public SprintDTO getSprintDTO() {
		return new SprintDTO();
	}

	@RequestMapping("/new")
	public String displayForm() {
		return Views.SPRINT_NEW;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String createTeam(@Valid SprintDTO sprintDTO, BindingResult result, RedirectAttributes redirectAttr) {
		String resultString = Views.SPRINT_NEW;

		if (!result.hasErrors()) {
			try {
				getSprintService().create(sprintDTO);
				String msg = getMessage(I18nKeys.CREATE_SUCCESS, getMessage(I18nKeys.SPRINT));
				redirectAttr.addFlashAttribute(Constants.SUCCESS_MESSAGE_KEY, msg);
				resultString = "redirect:/admin/sprints";
			} catch (DataIntegrityViolationException e) {
				result.addError(new FieldError("sprintDTO", "name", "Must be Unique"));
			}
		}

		return resultString;
	}

}
