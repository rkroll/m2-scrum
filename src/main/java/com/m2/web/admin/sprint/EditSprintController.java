package com.m2.web.admin.sprint;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.m2.sprint.dto.SprintDTO;
import com.m2.sprint.model.Sprint;
import com.m2.system.SystemSettingsService;
import com.m2.system.model.SystemSettings;
import com.m2.web.Constants;
import com.m2.web.Views;
import com.m2.web.common.I18nKeys;

@Controller
@RequestMapping("/admin/sprints")
public class EditSprintController extends AbstractSprintController {
	
	@Autowired
	private SystemSettingsService systemSettingsService;
	
	@ModelAttribute("sprintDTO")
	public SprintDTO getSprintDTO(@PathVariable("id") Long id) {
		Sprint sprint = getSprintService().getOne(id);
		SystemSettings settings = systemSettingsService.getSystemSettings();

		SprintDTO dto = new SprintDTO(sprint);

		if (settings.getCurrentSprint().equals(sprint)) {
			dto.setMakeCurrent(true);
		}

		return dto;
	}
	
	@RequestMapping("/{id}")
	public String displayForm() {
		return Views.SPRINT_EDIT;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String createTeam(@Valid SprintDTO sprintDTO, BindingResult result, RedirectAttributes redirectAttr) {
		String resultString = Views.TEAM_EDIT;

		if (!result.hasErrors()) {
			try {
				getSprintService().save(sprintDTO);
				String msg = getMessage(I18nKeys.UPDATE_SUCCESS, getMessage(I18nKeys.SPRINT));
				redirectAttr.addFlashAttribute(Constants.SUCCESS_MESSAGE_KEY, msg);
				resultString = "redirect:/admin/sprints";
			} catch (DataIntegrityViolationException e) {
				result.addError(new FieldError("sprintDTO", "name", "Must be Unique"));
			}
		}

		return resultString;
	}
}
