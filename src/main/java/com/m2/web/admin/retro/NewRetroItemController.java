package com.m2.web.admin.retro;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.m2.retro.RetrospectiveService;
import com.m2.retro.dto.RetroItemDTO;
import com.m2.web.AbstractBaseController;
import com.m2.web.Constants;
import com.m2.web.Views;
import com.m2.web.common.I18nKeys;

@Controller
@RequestMapping("/admin/retro-items")
public class NewRetroItemController extends AbstractBaseController {
	
	@Autowired
	private RetrospectiveService retrospectiveService;

	public NewRetroItemController() {
		super(Constants.ADMIN_SECTION);
	}
	
	@ModelAttribute("retroItemDTO")
	public RetroItemDTO addDTO() {
		return new RetroItemDTO();
	}
	
	@RequestMapping("/new")
	public String displayForm() {
		return Views.RETRO_ITEM_NEW;
	}

	@RequestMapping(method = RequestMethod.POST)
	public String createTeam(@Valid RetroItemDTO retroItemDTO, BindingResult result, RedirectAttributes redirectAttr) {
		String resultString = Views.RETRO_ITEM_NEW;

		if (!result.hasErrors()) {
			try {
				retrospectiveService.createRetroItem(retroItemDTO);
				String msg = getMessage(I18nKeys.CREATE_SUCCESS, getMessage(I18nKeys.RETRO_ITEM));
				redirectAttr.addFlashAttribute(Constants.SUCCESS_MESSAGE_KEY, msg);
				resultString = "redirect:/admin/retro-items";
			} catch (DataIntegrityViolationException e) {
				result.addError(new FieldError("retroItemDTO", "name", "Must be Unique"));
			}
		}

		return resultString;
	}

}
