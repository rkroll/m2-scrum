package com.m2.web.admin.retro;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.m2.retro.RetrospectiveService;
import com.m2.retro.model.RetrospectiveItem;
import com.m2.web.AbstractBaseController;
import com.m2.web.Constants;
import com.m2.web.Views;

@Controller
@RequestMapping("/admin/retro-items")
public class RetroItemOverviewController extends AbstractBaseController {

	@Autowired
	private RetrospectiveService retrospectiveService;

	public RetroItemOverviewController() {
		super(Constants.RETRO_ITEM_SECTION);
	}

	@ModelAttribute("items")
	public List<RetrospectiveItem> addItems() {
		return retrospectiveService.findActive();
	}

	@RequestMapping
	public String displayOverview() {
		return Views.RETRO_ITEM_OVERVIEW;
	}

}
