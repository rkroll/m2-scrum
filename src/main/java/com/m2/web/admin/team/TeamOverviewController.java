package com.m2.web.admin.team;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.m2.team.model.Team;
import com.m2.web.Views;

@Controller
@RequestMapping("/admin/teams")
public class TeamOverviewController extends AbstractTeamController {

	@ModelAttribute("teams")
	public List<Team> addTeams() {
		return getTeamService().findAll();
	}

	@RequestMapping
	public String overview() {
		return Views.TEAM_OVERVIEW;
	}
}
