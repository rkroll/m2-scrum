package com.m2.web.admin.team;

import org.springframework.beans.factory.annotation.Autowired;

import com.m2.team.TeamService;
import com.m2.web.AbstractBaseController;
import com.m2.web.Constants;

public class AbstractTeamController extends AbstractBaseController {
	public AbstractTeamController() {
		super(Constants.TEAM_SECTION);
	}

	@Autowired
	private TeamService teamService;

	public TeamService getTeamService() {
		return teamService;
	}

	public void setTeamService(TeamService teamService) {
		this.teamService = teamService;
	}

}
