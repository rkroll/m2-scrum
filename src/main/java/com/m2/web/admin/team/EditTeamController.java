package com.m2.web.admin.team;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.m2.profile.UserService;
import com.m2.profile.model.User;
import com.m2.team.model.Team;
import com.m2.web.Constants;
import com.m2.web.Views;
import com.m2.web.common.I18nKeys;

@PreAuthorize("hasRole('ROLE_ADMIN')")
@Controller
@RequestMapping("/admin/teams")
public class EditTeamController extends AbstractTeamController {
	@Autowired
	private UserService userService;

	@ModelAttribute("teamForm")
	public TeamForm getForm(@PathVariable("id") Long id) {
		Team team = getTeamService().getOne(id);
		return new TeamForm(team);
	}

	@ModelAttribute("users")
	public List<User> getUsers() {
		return getUserService().findActive();
	}

	@RequestMapping("/{id}")
	public String displayForm() {
		return Views.TEAM_EDIT;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.POST)
	public String createTeam(@Valid TeamForm teamForm, BindingResult result, RedirectAttributes redirectAttr) {
		String resultString = Views.TEAM_EDIT;

		if (!result.hasErrors()) {
			try {
				getTeamService().save(teamForm);
				String msg = getMessage(I18nKeys.UPDATE_SUCCESS, getMessage(I18nKeys.TEAM));
				redirectAttr.addFlashAttribute(Constants.SUCCESS_MESSAGE_KEY, msg);
				resultString = "redirect:/admin/teams";
			} catch (DataIntegrityViolationException e) {
				result.addError(new FieldError("newTeamForm", "name", "Must be Unique"));
			}
		}

		return resultString;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
