package com.m2.web.admin.team;

import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;

import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.util.Assert;

import com.m2.team.model.Team;

public class TeamForm {

	public interface Creating extends Default {
	}

	public interface Updating extends Default {
	}

	// only valide presence of ID when validating updates
	@NotNull(groups = { Updating.class })
	Long id;

	@NotEmpty
	private String name;

	@NotNull
	private Long scrumMaster;

	public TeamForm() {
	}

	public TeamForm(Team team) {
		Assert.notNull(team, "A team is required");
		this.id = team.getId();
		this.name = team.getName();
		this.scrumMaster = team.getScrumMaster().getId();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getScrumMaster() {
		return scrumMaster;
	}

	public void setScrumMaster(Long scrumMaster) {
		this.scrumMaster = scrumMaster;
	}
}
