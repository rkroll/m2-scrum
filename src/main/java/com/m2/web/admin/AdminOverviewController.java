package com.m2.web.admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.m2.web.AbstractBaseController;
import com.m2.web.Constants;

@Controller
public class AdminOverviewController extends AbstractBaseController {

	public AdminOverviewController() {
		super(Constants.ADMIN_SECTION);
	}
	
	@RequestMapping("/admin")
	public String displayOverview() {
		return "/admin/overview";
	}

}
