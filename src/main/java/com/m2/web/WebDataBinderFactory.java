package com.m2.web;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.springframework.web.bind.WebDataBinder;

public class WebDataBinderFactory {
	public static void registerDateCustomEditor(WebDataBinder binder, final TimeZone timeZone) {
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			public void setAsText(String value) {
				try {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					sdf.setTimeZone(timeZone);
					setValue(sdf.parse(value));
				} catch (ParseException e) {
					setValue(null);
				}
			}

			public String getAsText() {
				if (getValue() != null) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
					sdf.setTimeZone(timeZone);
					return sdf.format((Date) getValue());
				} else {
					return null;
				}
			}

		});
	}
}
