package com.m2.web;

public class Views {
	public static final String USER_OVERVIEW = "/admin/users/overview";
	public static final String USER_NEW = "/admin/users/new_user";
	public static final String USER_EDIT = "/admin/users/edit_user";
	
	public static final String TEAM_OVERVIEW = "/admin/teams/overview";
	public static final String TEAM_NEW = "/admin/teams/new_team";
	public static final String TEAM_EDIT = "/admin/teams/edit_team";
	
	public static final String SPRINT_OVERVIEW = "/admin/sprints/overview";
	public static final String SPRINT_NEW = "/admin/sprints/new_sprint";
	public static final String SPRINT_EDIT = "/admin/sprints/edit_sprint";
	
	public static final String RETRO_ITEM_OVERVIEW = "/admin/retro/retro_item_overview";
	public static final String RETRO_ITEM_NEW = "/admin/retro/new_retro_item";
}
