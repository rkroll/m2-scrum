package com.m2.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.m2.retro.RetrospectiveService;
import com.m2.sprint.SprintService;
import com.m2.sprint.model.Sprint;

@Controller
public class DashboardController {

	@Autowired
	private SprintService sprintService;

	@Autowired
	private RetrospectiveService retrospectiveService;

	@ModelAttribute("currentSprint")
	public Sprint currentSprint() {
		return sprintService.getCurrentSprint();
	}

	@ModelAttribute("hasReviewedCurrentSprint")
	public Boolean hasReviewedCurrentSprint() {
		return retrospectiveService.reviewedSprint(currentSprint());
	}

	@RequestMapping("/")
	public String getDashboard() {
		return "dashboard";
	}

	public SprintService getSprintService() {
		return sprintService;
	}

	public void setSprintService(SprintService sprintService) {
		this.sprintService = sprintService;
	}

}
