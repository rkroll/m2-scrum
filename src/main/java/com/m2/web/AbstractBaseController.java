package com.m2.web;

import java.util.TimeZone;

import org.springframework.context.MessageSource;
import org.springframework.context.MessageSourceAware;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;

public class AbstractBaseController implements MessageSourceAware {

	private MessageSource messageSource;

	private String section;

	public AbstractBaseController(String section) {
		this.section = section;
	}

	@ModelAttribute("section")
	public String section() {
		return this.section;
	}

	@InitBinder
	public void binder(WebDataBinder binder) {
		// FIXME: add user specific TZ
		WebDataBinderFactory.registerDateCustomEditor(binder, TimeZone.getTimeZone("America/New York"));
	}

	public String getMessage(String key) {
		return getMessageSource().getMessage(key, new Object[] {}, LocaleContextHolder.getLocale());
	}

	public String getMessage(String key, Object value) {
		return getMessageSource().getMessage(key, new Object[] { value }, LocaleContextHolder.getLocale());
	}

	@Override
	public void setMessageSource(MessageSource messageSource) {
		this.messageSource = messageSource;
	}

	public MessageSource getMessageSource() {
		return messageSource;
	}

}
