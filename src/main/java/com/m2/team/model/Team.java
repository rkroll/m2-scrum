package com.m2.team.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.util.Assert;

import com.google.common.collect.Lists;
import com.m2.core.model.AbstractModel;
import com.m2.profile.model.User;

@Entity
@Table(name = "teams")
public class Team extends AbstractModel {

	private static final long serialVersionUID = 7289462790327196084L;
	
	private String name;
	private User scrumMaster;
	private List<User> users = Lists.newArrayList();

	Team() {
		// for hibernate
	}
	
	public Team(String name, User scrumMaster) {
		Assert.notNull(name, "Name is required");
		Assert.notNull(scrumMaster, "Scrum master is required");
		this.name = name;
		this.scrumMaster = scrumMaster;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@OneToOne
	public User getScrumMaster() {
		return scrumMaster;
	}

	public void setScrumMaster(User scrumMaster) {
		this.scrumMaster = scrumMaster;
	}

	@OneToMany
	@JoinTable(name = "team_user", joinColumns = { @JoinColumn(name = "team_id") }, inverseJoinColumns = { @JoinColumn(name = "user_id") })
	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((scrumMaster == null) ? 0 : scrumMaster.hashCode());
		result = prime * result + ((users == null) ? 0 : users.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Team other = (Team) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (scrumMaster == null) {
			if (other.scrumMaster != null)
				return false;
		} else if (!scrumMaster.equals(other.scrumMaster))
			return false;
		if (users == null) {
			if (other.users != null)
				return false;
		} else if (!users.equals(other.users))
			return false;
		return true;
	}

}
