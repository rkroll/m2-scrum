package com.m2.team;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.m2.profile.model.User;
import com.m2.profile.repository.UserRepository;
import com.m2.team.model.Team;
import com.m2.team.repository.TeamRepository;
import com.m2.web.admin.team.TeamForm;

@Service
@Transactional
public class TeamService {
	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private UserRepository userRepository;

	public Team createTeam(TeamForm teamFrom) {
		User scrumMaster = userRepository.getOne(teamFrom.getScrumMaster());
		Team newTeam = new Team(teamFrom.getName(), scrumMaster);
		return teamRepository.save(newTeam);
	}
	
	public Team save(TeamForm teamForm) {
		Team team = teamRepository.getOne(teamForm.getId());
		BeanUtils.copyProperties(teamForm, team, "id", "scrumMaster");
		User scrumMaster = userRepository.getOne(teamForm.getScrumMaster());
		team.setScrumMaster(scrumMaster);
		teamRepository.save(team);
		return team;
	}

	public TeamRepository getTeamRepository() {
		return teamRepository;
	}

	public void setTeamRepository(TeamRepository teamRepository) {
		this.teamRepository = teamRepository;
	}

	public List<Team> findAll() {
		return teamRepository.findAll();
	}

	public Team getOne(Long id) {
		return teamRepository.getOne(id);
	}
}
