package com.m2.jersey.jackson;

import java.text.SimpleDateFormat;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

import org.codehaus.jackson.Version;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.introspect.JacksonAnnotationIntrospector;
import org.codehaus.jackson.map.module.SimpleModule;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

@Provider
public class ObjectMapperProvider implements ContextResolver<ObjectMapper>{

	public ObjectMapper getContext() {
		return getContext(null);
	}

	@SuppressWarnings("deprecation")
	@Override
	public ObjectMapper getContext(Class<?> ignored) {
		// create the objectMapper.
		ObjectMapper objectMapper = new ObjectMapper();

		// configure the object mapper - dates as timestamps
		//objectMapper.getSerializationConfig().setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZ"));
		objectMapper.getSerializationConfig().withDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZZ"));
		objectMapper.configure(SerializationConfig.Feature.WRITE_DATES_AS_TIMESTAMPS, false);
		objectMapper.configure(SerializationConfig.Feature.INDENT_OUTPUT, true);

		objectMapper.setPropertyNamingStrategy(new CStyleNamingStrategy());

		AnnotationIntrospector primary = new JacksonAnnotationIntrospector();
		AnnotationIntrospector secondary = new JaxbAnnotationIntrospector();
		AnnotationIntrospector pair = new AnnotationIntrospector.Pair(primary, secondary);

		objectMapper.getSerializationConfig().withAnnotationIntrospector(pair);
		objectMapper.getSerializationConfig().setAnnotationIntrospector(pair);

		SimpleModule customModule = new SimpleModule("CustomModule", new Version(1,0,0,null));
		
		objectMapper.registerModule(customModule);

		objectMapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

		return objectMapper;
	}

	public ObjectMapper getStrictContext() {
		ObjectMapper mapper = getContext();
		// only include items marked with a view
		mapper.configure(SerializationConfig.Feature.DEFAULT_VIEW_INCLUSION, false);
		
		return mapper;
	}

}         