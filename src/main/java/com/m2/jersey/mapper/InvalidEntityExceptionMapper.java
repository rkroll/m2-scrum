package com.m2.jersey.mapper;

import java.io.StringWriter;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.m2.validation.InvalidEntityException;

@Provider
public class InvalidEntityExceptionMapper implements ExceptionMapper<InvalidEntityException> {
	private static final Logger LOGGER = LoggerFactory.getLogger(InvalidEntityExceptionMapper.class);

	private static final int UNPROCESSABLE_ENTITY = 422;

	@Context
	private HttpServletRequest request;

	@Override
	public Response toResponse(InvalidEntityException exception) {
		final StringWriter writer = new StringWriter(4096);
		try {
			writer.write(exception.getMessage());
			writer.write("\n");
			for (String error : exception.getErrors()) {
				writer.write(error);
				writer.write("\n");
			}
		}
		catch (Exception e) {
			LOGGER.warn("Unable to generate error page", e);
		}

		return Response.status(UNPROCESSABLE_ENTITY).type(MediaType.TEXT_HTML_TYPE).entity(writer.toString()).build();
	}
}
