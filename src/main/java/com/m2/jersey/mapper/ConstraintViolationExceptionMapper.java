package com.m2.jersey.mapper;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.google.common.collect.Lists;
import com.m2.jersey.jackson.CStyleNamingStrategy;

@Provider
@Component
public class ConstraintViolationExceptionMapper implements ExceptionMapper<ConstraintViolationException> {
	
	private CStyleNamingStrategy namingStrategy = new CStyleNamingStrategy();
	
	@Override
	public Response toResponse(ConstraintViolationException exception) {
		
		List<String> errors = Lists.newArrayList();
		
		Set<ConstraintViolation<?>> violations = exception.getConstraintViolations();
		
		for(ConstraintViolation<?> v : violations) {
			String path = namingStrategy.convert(v.getPropertyPath().toString());
			StringBuilder sb = new StringBuilder();
			
			if(StringUtils.isNotBlank(path)) {
				sb.append(String.format("%s: ", path));
			}
			
			sb.append(v.getMessage());
			
			errors.add(sb.toString()); 
		}
		
		return Response.status(Response.Status.BAD_REQUEST)
                .type(MediaType.APPLICATION_JSON)
                .entity(errors)
                .build();
	}

}
