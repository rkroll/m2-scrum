package com.m2.jersey.mapper;

import java.io.StringWriter;
import java.util.Random;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Provider
public class LoggingExceptionMapper implements ExceptionMapper<Throwable> {
	private static final Logger LOGGER = LoggerFactory.getLogger(LoggingExceptionMapper.class);

	private static final Random RANDOM = new Random();

	@Context
	private HttpServletRequest request;

	@Override
	public Response toResponse(Throwable exception) {
		if (exception instanceof WebApplicationException) {
			return ((WebApplicationException) exception).getResponse();
		}

		final StringWriter writer = new StringWriter(4096);
		try {
			final long id = randomId();
			logException(id, exception);

			writer.write(formatResponseEntity(id, exception));
		}
		catch (Exception e) {
			LOGGER.warn("Unable to generate error page", e);
		}

		return Response.serverError().type(MediaType.TEXT_PLAIN).entity(writer.toString()).build();
	}

	protected void logException(long id, Throwable exception) {
		LOGGER.error(formatLogMessage(id, exception), exception);
	}

	protected String formatResponseEntity(long id, Throwable exception) {
		return String.format("There was an error processing your request. It has been logged (ID %016x).\n", id);
	}

	protected String formatLogMessage(long id, Throwable exception) {
		return String.format("Error handling a request: %016x", id);
	}

	protected static long randomId() {
		return RANDOM.nextLong();
	}
}