package com.m2.config;

import java.util.ArrayList;
import java.util.Collection;

import javax.sql.DataSource;

import org.flywaydb.core.Flyway;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;

import com.m2.validation.Validator;

@Configuration
@ComponentScan(basePackages = "com.m2")
@ConfigurationProperties(prefix = "ema")
@EnableConfigurationProperties
@EnableCaching
public class EmaConfig implements InitializingBean {
	private static final Logger logger = LoggerFactory.getLogger(EmaConfig.class);
	
	@Autowired
	private DataSource ds;

	@Override
	public void afterPropertiesSet() throws Exception {
		// todo - load banner
		Flyway flyway = new Flyway();
		flyway.setDataSource(ds);
		flyway.migrate();
	}
	
	@Bean(name = "validator")
	public LocalValidatorFactoryBean validator() {
	    return new LocalValidatorFactoryBean();
	}
	
	@Bean
	public Validator getValidator() {
		return new Validator(validator());
	}
	
	@Bean
	public CacheManager getCacheManager() {
		SimpleCacheManager mgr = new SimpleCacheManager();
		Collection<Cache> caches = new ArrayList<Cache>();

		caches.add(createCache("drugs"));
		caches.add(createCache("test"));

		mgr.setCaches(caches);

		return mgr;
	}

	private Cache createCache(String name) {
		return new ConcurrentMapCache(name, false);
	}

}
