package com.m2.config;

import java.sql.SQLException;

import org.h2.tools.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * This config class controls startup of components which are useful in
 * development mode only (mock SMTP server, embedded memcached, etc.)
 * 
 * @author rich.kroll
 * 
 */
@Configuration
@ComponentScan(basePackages = "com.m2")
@ConfigurationProperties(prefix = "ema")
@ConditionalOnExpression("${ema.dev} == true")
@EnableConfigurationProperties
public class EmaDeveloperConfig implements InitializingBean {
	private static final Logger logger = LoggerFactory.getLogger(EmaDeveloperConfig.class);

	@Override
	public void afterPropertiesSet() throws Exception {
		logger.info("*** BOOTING DEVELOPMENT MODE ***");
	}


	@Bean
	public Server h2ConsoleWebServer() throws SQLException {
		Server server = Server.createWebServer("-webPort", "8082");
		server.start();
		return server;
	}
}
