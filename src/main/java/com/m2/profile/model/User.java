package com.m2.profile.model;

import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlTransient;

import org.apache.commons.lang.StringUtils;
import org.hibernate.validator.constraints.Email;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.m2.core.model.AbstractModel;
import com.m2.validation.ValidUsername;

@Entity
@Table(name = "users", uniqueConstraints = { @UniqueConstraint(columnNames = "username") })
public class User extends AbstractModel implements UserDetails {

	private static final long serialVersionUID = -7966248413060156708L;

	@NotNull
	@Email
	private String email;

	@ValidUsername
	private String username;

	@NotNull
	private String name;

	@NotNull(message = "password.null")
	@Size(min = 3, max = 255, message = "password.length")
	private String password;

	private boolean active = true;

	private boolean credentialsActive = true;

	private boolean enabled = true;

	private Set<Role> roles = new LinkedHashSet<Role>();

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = StringUtils.lowerCase(username);
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEmail() {
		return email;
	}

	@XmlTransient
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@ManyToMany
	@JoinTable(name = "user_role", joinColumns = { @JoinColumn(name = "user_id") }, inverseJoinColumns = { @JoinColumn(name = "role_id") })
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Transient
	public void addRole(Role role) {
		this.roles.add(role);
	}

	@Override
	@Transient
	@XmlTransient
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return roles;
	}

	public boolean hasRole(String role) {
		boolean hasRole = false;

		for (GrantedAuthority r : getAuthorities()) {
			if (r.getAuthority().equals(role)) {
				hasRole = true;
			}
		}

		return hasRole;
	}

	public boolean hasRole(Role role) {
		final boolean hasRole;

		if (getAuthorities().contains(role)) {
			hasRole = true;
		} else {
			hasRole = false;
		}

		return hasRole;
	}

	@Override
	@Transient
	public boolean isAccountNonExpired() {
		return active;
	}

	@Override
	@Transient
	public boolean isAccountNonLocked() {
		return active;
	}

	@Override
	@Transient
	public boolean isCredentialsNonExpired() {
		return credentialsActive;
	}

	@Override
	public boolean isEnabled() {
		return enabled;
	}

	/**
	 * @param enabled
	 *            the enabled to set
	 */
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + (credentialsActive ? 1231 : 1237);
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + (enabled ? 1231 : 1237);
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((roles == null) ? 0 : roles.hashCode());
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (active != other.active)
			return false;
		if (credentialsActive != other.credentialsActive)
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (enabled != other.enabled)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (roles == null) {
			if (other.roles != null)
				return false;
		} else if (!roles.equals(other.roles))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [username=").append(username).append(", getId()=").append(getId()).append("]");
		return builder.toString();
	}

	public boolean hasRole(RoleEnum r) {

		for (Role role : roles) {
			if (role.getAuthority().equals(r.name())) {
				return true;
			}
		}
		return false;
	}

	public void removeRole(Role role) {
		this.roles.remove(role);
	}

}
