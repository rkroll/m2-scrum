package com.m2.profile.model;

public enum RoleEnum {
	ROLE_ADMIN,
	ROLE_USER,
	ROLE_SCRUM_MASTER
}