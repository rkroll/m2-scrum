package com.m2.profile.repository;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.m2.profile.model.Role;

public class RoleSpecifications {
	public static Specification<Role> nameIn(final List<String> roleNames) {
		return new Specification<Role>() {

			@Override
			public Predicate toPredicate(Root<Role> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
	            return root.get("name").in(roleNames);
			}
		};
	}
}
