package com.m2.profile.repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.m2.profile.model.User;

public class UserSpecifications {
	public static Specification<User> usernameIs(final String name) {
		return new Specification<User>() {
			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.equal(root.get("username"), name);
			}
		};
	}

	public static Specification<User> active() {
		return new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {

				Predicate active = cb.equal(root.get("active"), true);
				Predicate locked = cb.equal(root.get("enabled"), true);

				return cb.and(active, locked);
			}
		};
	}

	public static Specification<User> notAdmin() {
		return new Specification<User>() {

			@Override
			public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.notEqual(root.get("username"), "admin");
			}
		};
	}
}
