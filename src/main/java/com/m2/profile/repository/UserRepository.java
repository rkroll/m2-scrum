package com.m2.profile.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.m2.profile.model.User;

public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

	User findByUsername(String username);

	// @Query("select ds from DrugSwitch ds where ds.to = :drug")
	// List<DrugSwitch> findSwitchesTo(@Param("drug") Drug drug);
}
