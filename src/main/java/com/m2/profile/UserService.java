package com.m2.profile;

import static com.m2.profile.repository.UserSpecifications.active;
import static com.m2.profile.repository.UserSpecifications.notAdmin;
import static org.springframework.data.jpa.domain.Specifications.where;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import com.m2.profile.model.Role;
import com.m2.profile.model.User;
import com.m2.profile.repository.RoleRepository;
import com.m2.profile.repository.RoleSpecifications;
import com.m2.profile.repository.UserRepository;
import com.m2.web.admin.user.UserForm;

@Service
@Transactional
public class UserService {

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	public User findByUsername(String username) {
		return userRepository.findByUsername(username);
	}

	public List<User> findActive() {
		return userRepository.findAll(where(notAdmin()).and(active()));
	}

	public List<User> findAll() {
		return userRepository.findAll(notAdmin());
	}

	public User findOne(Long id) {
		return userRepository.findOne(id);
	}

	public User create(UserForm form) {
		Assert.notNull(form, "New user form is required");
		User user = new User();
		user.setEmail(form.getEmail());
		user.setUsername(form.getUsername());
		user.setName(form.getName());
		user.setPassword(getPasswordEncoder().encode("changeme"));

		List<Role> roles = roleRepository.findAll(RoleSpecifications.nameIn(form.getRoles()));

		for (Role role : roles) {
			user.addRole(role);
		}

		return userRepository.save(user);
	}

	public User save(UserForm userForm) {
		User user = userRepository.getOne(userForm.getId());

		BeanUtils.copyProperties(userForm, user, "roles", "id", "password");

		List<Role> roles = roleRepository.findAll(RoleSpecifications.nameIn(userForm.getRoles()));

		user.getRoles().clear();

		for (Role role : roles) {
			user.addRole(role);
		}

		return user;
	}
	
	public User getCurrentUser() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth == null) {
			throw new IllegalStateException("Authentication not present in security context");
		}

		if (auth.getPrincipal() instanceof UserDetails) {
			UserDetails details = (org.springframework.security.core.userdetails.UserDetails) auth.getPrincipal();
			return findByUsername(details.getUsername());
		}
		else if (auth.getPrincipal() instanceof String) {
			return findByUsername((String) auth.getPrincipal());
		}
		else {
			throw new IllegalStateException("User must be fully authenticated");
		}

	} 

	public UserRepository getUserRepository() {
		return userRepository;
	}

	public void setUserRepository(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public RoleRepository getRoleRepository() {
		return roleRepository;
	}

	public void setRoleRepository(RoleRepository roleRepository) {
		this.roleRepository = roleRepository;
	}

	public PasswordEncoder getPasswordEncoder() {
		return passwordEncoder;
	}

	public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
		this.passwordEncoder = passwordEncoder;
	}

	public List<Role> getRoles() {
		return roleRepository.findAll();
	}
}
