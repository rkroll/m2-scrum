package com.m2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.orm.jpa.support.OpenEntityManagerInViewFilter;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.rememberme.TokenBasedRememberMeServices;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import com.sun.jersey.spi.spring.container.servlet.SpringServlet;

@Configuration
@ComponentScan(basePackages = { "com.m2.config" })
@EnableAutoConfiguration
@EnableTransactionManagement
@EnableGlobalMethodSecurity(prePostEnabled=true)
public class M2ScrumWebApplication extends WebMvcConfigurerAdapter {

	private static final String REMEMBER_ME_TOKEN_KEY = "ABC123";

	public static void main(String[] args) throws Exception {
		new SpringApplicationBuilder(M2ScrumWebApplication.class).run(args);
	}

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addViewController("/login").setViewName("login");
	}

	@Bean
	public UserDetailsService userDetailsService() {
		return new com.m2.profile.UserDetailsServiceAdapter();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public ApplicationSecurity applicationSecurity() {
		return new ApplicationSecurity(rememberMeServices(), userDetailsService(), passwordEncoder());
	}

	@Bean
	public RememberMeServices rememberMeServices() {
		// This should be changed to PersistentTokenBasedRememberMeServices for cluster use
		TokenBasedRememberMeServices rememberMeServices = new TokenBasedRememberMeServices(REMEMBER_ME_TOKEN_KEY, userDetailsService());
		rememberMeServices.setCookieName("m2Scrum");
		rememberMeServices.setParameter("rememberMe");
		return rememberMeServices;
	}

	@Bean
	public OpenEntityManagerInViewFilter getOpenEntityManagerInViewFilter() {
		return new OpenEntityManagerInViewFilter();
	}

	@Bean
	public ServletRegistrationBean jerseyServletRegistration() {
		SpringServlet servlet = new SpringServlet();
		ServletRegistrationBean registration = new ServletRegistrationBean(servlet);
		Map<String, String> params = new HashMap<String, String>();
		params.put("com.sun.jersey.api.json.POJOMappingFeature", "true");
		params.put("com.sun.jersey.config.property.packages", "com.m2");
		params.put("com.sun.jersey.spi.container.ContainerRequestFilters",
				"com.sun.jersey.api.container.filter.LoggingFilter, com.sun.jersey.api.container.filter.GZIPContentEncodingFilter");
		params.put("com.sun.jersey.spi.container.ContainerResponseFilters",
				"com.sun.jersey.api.container.filter.LoggingFilter, com.sun.jersey.api.container.filter.GZIPContentEncodingFilter");
		params.put("jersey.config.server.tracing.type", "ALL");
		params.put("jersey.config.server.tracing.threshold", "TRACE");
		registration.setInitParameters(params);
		registration.setUrlMappings(new ArrayList<String>(Arrays.asList("/api/*")));

		return registration;
	}

	@Order(Ordered.LOWEST_PRECEDENCE - 8)
	protected static class ApplicationSecurity extends WebSecurityConfigurerAdapter {
		private UserDetailsService userDetailsService;
		private PasswordEncoder passwordEncoder;
		private RememberMeServices rememberMeServices;

		public ApplicationSecurity(RememberMeServices rememberMeServices, UserDetailsService userDetailsService,
				PasswordEncoder passwordEncoder) {
			super();
			this.userDetailsService = userDetailsService;
			this.passwordEncoder = passwordEncoder;
			this.rememberMeServices = rememberMeServices;
		}

		@Override
		protected void configure(HttpSecurity http) throws Exception {
			http.csrf().disable();
			http.rememberMe().rememberMeServices(rememberMeServices).key(REMEMBER_ME_TOKEN_KEY);
			http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
			http.authorizeRequests()
				.anyRequest()
					.authenticated()
					.and()
						.formLogin()
						.loginPage("/login")
						.failureUrl("/login?error")
						.permitAll()
					.and()
						.authorizeRequests()
						.antMatchers("/admin/**")
						.hasRole("ADMIN")
					.and()
						.httpBasic();
		}

		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
		}

		@Override
		public UserDetailsService userDetailsServiceBean() {
			return userDetailsService;
		}

	}

}