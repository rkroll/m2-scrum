package com.m2.validation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Target({ ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = { ValidDateRangeValidator.class })
@Documented
public @interface ValidDateRange {
	String message() default "{valid_date_range.constraint.failed}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
