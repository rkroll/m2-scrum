package com.m2.validation;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.*;

import javax.validation.Constraint;
import javax.validation.Payload;


@Target({ METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = TodayOrFutureValidator.class)
@Documented
public @interface TodayOrFuture {
    String message() default "{today_or_future.constraint.failed}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
        