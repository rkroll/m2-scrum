package com.m2.validation;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UsernameValidationConstraint implements ConstraintValidator<ValidUsername, String> {

	@SuppressWarnings("unused")
	private String message;

	// any lowercase letter, number, and the following special chars: . @ _ -
	private static final String PATTERN_STRING = "^[a-zA-Z0-9\\.@_-]+$";

	private Pattern pattern = Pattern.compile(PATTERN_STRING);

	@Override
	public void initialize(ValidUsername constraintAnnotation) {
		message = constraintAnnotation.message();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		boolean valid = false;

		if (value != null) {
			valid = pattern.matcher(value).matches();
		}
		return valid;
	}
}
