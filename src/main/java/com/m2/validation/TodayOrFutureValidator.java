package com.m2.validation;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang.time.DateUtils;

public class TodayOrFutureValidator implements ConstraintValidator<TodayOrFuture, Date> {

	@Override
	public void initialize(TodayOrFuture constraintAnnotation) {

	}

	@Override
	public boolean isValid(Date value, ConstraintValidatorContext context) {
		// Use a required validator to test for existence
		if(value == null) {
			return true;
		}

		Date today = new Date();
		return (DateUtils.isSameDay(value, today) || value.after( today ));
	}

}
          