package com.m2.validation;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.m2.core.model.DateRangeAware;

/**
 * Validates a valid date range.
 * 
 * @author richkroll
 * 
 */
public class ValidDateRangeValidator implements ConstraintValidator<ValidDateRange, DateRangeAware> {

	@Override
	public void initialize(ValidDateRange constraintAnnotation) {

	}

	@Override
	public boolean isValid(DateRangeAware value, ConstraintValidatorContext context) {
		Date startDate = value.getStartDate();
		Date endDate = value.getEndDate();

		if ((startDate == null || endDate == null)
				|| (startDate != null && endDate == null)) {
			return true;
		} else {
			return startDate.before(endDate);
		}

	}

}
